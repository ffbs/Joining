#!/usr/bin/python3
import sys
import argparse
import subprocess
import socket
import gzip 
import json
import time
import threading 
import alfred
import batadv
import copy
#import alfred_to_influxdb as db


def client(config, channels):
    address = {'ip' : config['server_address']}
    address['port'] = config['server_port'] 
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    alfred.sendData(channels,sock,address)
    batadv.sendData(config['interface'], sock, address)
    
def alfred_cleaner(config, alfred_channels,batadv,alfredLock,batadvLock):
    count = 0
    while True:
        count += 1
        print("Aquire alfed for cleanup")
        alfredLock.acquire(True)
        print("Nice!")
        for channel in alfred_channels.keys():
            #print('a')
            to_del = []
            for node in alfred_channels[channel].keys():
                #print('b')
                try:
                    if time.time() - alfred_channels[channel][node]['time'] > config['update_interval'] * 2:
                        to_del.append(node)
                except KeyError as e:
                    print(e)
                    print("no time in: "+ str(alfred_channels[channel][node]))
                    alfred_channels[channel][node]['time'] = time.time()

            for node in to_del:
                del alfred_channels[channel][node]
            # wait 5 min befor writing new data into influxdb
            #if count % 30 == 0:
            #    db.toInfluxdb('', hostname='localhost', port=8086, username='freifunk', password='freifunk', database='freifunk',jsond=alfred_channels[channel])
        #print("worked all channels")
        f = open(config['alfred_out'],'w') 
        #print("opened file")
        json.dump(alfred_channels,f)
        #print("json dumped")
        f.close()
        #print("file closed")
        alfredLock.release()
        print("Release alfed")

        print("Aquireing bamän..")
        batadvLock.acquire(True)
        #print("ok =)")
        new = list(filter(lambda entry: time.time() - entry['time'] > config['update_interval'] * 2, batadv))
        for entry in new:
            batadv.remove(entry)

        f = open(config['batadv_out'],'w') 
        output = ""
        for entry in batadv:
           output += entry['data']+'\n'
        #print("writing data")
        #print(output)
        f.write(output)
        f.close()
        batadvLock.release()
        print("Release bamän..")

        time.sleep(10)

def alfred_server(config, alfred_channels,batadv_data,alfredLock,batadvLock):
    UDP_IP = config['server_bind_to']
    UDP_PORT = config['server_port'] 
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.bind((UDP_IP, UDP_PORT))
    while True:
        data, addr = sock.recvfrom(10000)
        data = gzip.zlib.decompress(data)
        rdata = json.loads(data.decode('utf-8'))
        if rdata[0] == "alfred":
            print("got alfred data")
            alfredLock.acquire(True)
            channel = rdata[1]
            message = rdata[2]
            node = json.loads(message)
            if 'node_id' in node: #dependable way
                mac = node['node_id']
                mac = ':'.join([mac[i:i+2] for i in range(0,len(mac),2)])
            elif 'network' in node: #fallback
                try:
                    mac = node['network']['mac']
                except KeyError as e:
                    #print(node)
                    alfredLock.release()
                    continue
            else: #fuck you
                alfredLock.release()
                continue
            if str(channel) not in alfred_channels:
                alfred_channels[str(channel)] = {mac : node}
                alfred_channels[str(channel)][mac]['time'] = int(time.time())
            else:
                alfred_channels[str(channel)][mac] = node
                alfred_channels[str(channel)][mac]['time'] = int(time.time())
            alfredLock.release()

        elif rdata[0] == "batadv-vis":
            print("got batadv-vis data")
            batadvLock.acquire(True)
            line = rdata[1]
            entry = {'time': time.time()}
            entry['data'] = line
            batadv_data.append(entry)
            batadvLock.release()
        
        else:
            print('got unknown data')
    

def main():
    parser = argparse.ArgumentParser(description='This is to tunnel alfred and batadv-vis data over Layer3 networks.')
    parser.add_argument('-i', default=False, action='store_true', dest='client', help='enable data input mode (client)')
    parser.add_argument('-o', default=False, action='store_true', dest='server', help='enable data output mode (server)')
    parser.add_argument('-c', default="config.json", action='store', dest='configFile', help='config file')
    parser.add_argument('-r', default=[], action='append', dest='channels', help='alfred channels to transmit')
    args = parser.parse_args()
    alfredLock = threading.Lock()
    batadvLock = threading.Lock()

    try:
        f = open(args.configFile)
        config=json.load(f)
    except :
        #print('config file error')
        parser.print_help()
        exit(0)

    if args.client:
        print("client mode")
        channels = set()
        for c in config['channels']:
            channels.add(str(c)) 
        for c in args.channels:
            channels.add(c)
        client(config,channels)
        exit(0)

    if args.server:
        print("server mode")
        global_alfred_channels = {}
        batadv_data=[]
        threading.Thread(target=alfred_server, args=(config,global_alfred_channels,batadv_data,alfredLock,batadvLock)).start()
        threading.Thread(target=alfred_cleaner, args=(config,global_alfred_channels,batadv_data,alfredLock,batadvLock)).start()
        exit(0)

    try:
        if args.help:
            parser.print_help()
            exit(0)
    except AttributeError as e:
        parser.print_help()
        exit(0)

        
  
if __name__ == "__main__":
    main()
