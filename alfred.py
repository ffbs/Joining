import subprocess
import socket
import gzip 
import json
import time
import threading 

def sendData(channels, sock,serverAddress):
    for channel in channels:
        print("Sending data for Alfred-Channel: {}".format(channel))
        p = subprocess.Popen(['/usr/local/bin/alfred-json','-z','-r',channel, '-f', 'json'], stdout=subprocess.PIPE, stderr=subprocess.PIPE) 
        data, err = p.communicate()
        data = json.loads(data.decode("utf-8")).values()
        print("Alfred returned {} elements.".format(len(data)))
        for d in data:
            m=json.dumps(d)
            message = json.dumps(["alfred",str(channel), m])
            print("Alfred: {}".format(message))
            message = gzip.zlib.compress(bytes(message,'UTF-8'))
            sock.sendto(message, (serverAddress['ip'], serverAddress['port']))
