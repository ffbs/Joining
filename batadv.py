import subprocess
import socket
import gzip 
import json
import time
import threading 

def sendData(interface, sock,serverAddress):
    p = subprocess.Popen(['/usr/sbin/batadv-vis','-i',interface,'-f', 'json'], stdout=subprocess.PIPE, stderr=subprocess.PIPE) 
    data, err = p.communicate()
    print("Sending batadv-vis-data...")
    for line in data.splitlines():
        print("Sending line: {}".format(line))
        line = line.decode('utf-8')
        line = json.loads(line)
        message = json.dumps(["batadv-vis", json.dumps(line)])
        
        message = gzip.zlib.compress(bytes(message,'UTF-8'))
        sock.sendto(message, (serverAddress['ip'], serverAddress['port']))

if __name__ == "__main__":
    sendData('bat0',None,None)
